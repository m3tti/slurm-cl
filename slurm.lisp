(defpackage :slurm
  (:use :cl
        :wookie
        :cl-who
        :parenscript
        :flexi-streams
        :cl-json)
  (:shadowing-import-from :cl-json "PROTOTYPE")
  (:export :frontend
           :start-slurm
           :add-function
           :act))
(in-package :slurm)

(defvar *dispatch-fn* nil)

(defmacro prop (key data)
  `(cdr (assoc ,key ,data)))

(defun template (title)
  (with-html-output-to-string (s)
    (:html
     (:head
      (:title title)
      (:script :type "text/javascript"
               :src "/vendor.js")
      (:script :type "text/javascript"
               :src "/app.js")
      (:link :rel "stylesheet"
             :href "/tachyons.min.css"))
    (:body))))
  
(defun read-file (file-name)
  (car (with-open-file (stream file-name)
         (loop for line = (read-line stream nil)
            while line
            collect line))))

(defun decode-req (req)
  (let ((data (flexi-streams:octets-to-string req :external-format :utf-8)))
    (cl-json:decode-json-from-string data)))

(defun dispatch (req-data res-fn)
  (let* ((action (intern (string-upcase (prop :ACTION req-data)) "KEYWORD"))
         (payload (prop :PAYLOAD req-data))
         (dispatch-fn (getf *dispatch-fn* action)))
    (funcall dispatch-fn res-fn payload)))

(defun add-to-dispatch-functions (keyword fn)
  (setq *dispatch-fn* (append (list keyword fn) *dispatch-fn*)))

(defmacro add-function (keyword (res payload) body)
  `(add-to-dispatch-functions ,keyword
                              (lambda (response ,payload)
                                (flet ((,res (d) (funcall response d)))
                                  ,body))))

(defun attach-routes ()
  ;; define our homepage route
  (defroute (:get "/") (req res)
    (send-response res :body (template "test")))

  (defroute (:post "/dispatch") (req res)
    (let ((req-data (decode-req (request-body req)))
          (res-fn (lambda (payload)
                    (send-response res :body (cl-json:encode-json-to-string payload)))))
      (dispatch req-data res-fn)))

  (defroute (:get "/vendor.js") (req res)
    (send-response res :body (concatenate 'string
                                          (read-file "hyperapp.js")
                                          (read-file "hyperappFx.js"))))

  (defroute (:get "/tachyons.min.css") (req res)
    (send-response res :body (read-file "tachyons.min.css")))
  )


(defun start-slurm (port)
  (attach-routes)
  (as:with-event-loop ()
    ;; create a listener object, and pass it to start-server, which starts Wookie
    (let* ((listener (make-instance 'listener
                                    :bind nil  ; equivalent to "0.0.0.0" aka "don't care"
                                    :port port))
           ;; start it!! this passes back a cl-async server class
           (server (start-server listener)))

      ;; stop server on ctrl+c
      (as:signal-handler 2
                         (lambda (sig)
                           (declare (ignore sig))
                           (as:free-signal-handler 2)
                           (as:close-tcp-server server))))))

    
;;==== FRONTEND
(defpsmacro document-ready (fn)
  `(document.add-event-listener
    "DOMContentLoaded"
    (lambda (event)
      ,fn
      )))

(defmacro hyperapp (state actions view)
  `(ps (document-ready (let ((h hyperapp.h)
                             (json (getprop window '-J-S-O-N))
                             (happ hyperapp.app)
                             (with-fx hyperapp-fx.with-fx)
                             (action hyperapp-fx.action)
                             (http hyperapp-fx.http))
                     
                     (defun dispatch (action payload success error)
                       (http
                        "/dispatch"
                        success
                        (create
                         "method" "POST"
                         "body" (json.stringify (create
                                 "action" action
                                 "payload" payload))
                         "error" error)))
                     
                     (defun app (state actions view)
                       ((with-fx happ) state actions view document.body))
                     
                     (app ,state ,actions (lambda (state) ,view))))))

(defpsmacro act ((value state) body)
  `(lambda (,value)
     (lambda (,state)
       ,body)))
                     

(defun frontend (state actions view)
  (defroute (:get "/app.js") (req res)
    (send-response res
                   :body (hyperapp
                          (lisp state)
                          (lisp actions)
                          (lisp view)))))
