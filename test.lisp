(defpackage :test
  (:use :cl :slurm :parenscript))
(in-package :test)

;; backend functions
(add-function :ECHO (res payload)
              (res payload))

;; app 
(defun state ()
  '(create "autos" 22))

(defun test ()
  '(act (value state)
    (dispatch "echo" value "success" "error")))

(defun actions ()
  '(create
    "myAction" (lisp (test))
    "success" (act (value state)
               (progn
                 (console.log value)
                 value))
    "error" (act (value state)
             (progn
               (console.log value)
               value))))

(defun view ()
  '(h "a"
    (create "onclick" (action "myAction" (create "wurst" 22))
            "class" "ba")
      "Hello World"))

(slurm:frontend
 (state)
 (actions)
 (view))

(slurm:start-slurm 8080)
