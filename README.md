# Slurm
![Slurm](https://images.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.HQsTB5MgwvsAIGoLuRhWbgHaFW%26pid%3D15.1&f=1)

A highly addictive web framework to build applications.
It tries to make building single page apps in lisp easy.
Just plain lisp no fancy other stuff :D except the JS libs.

## Slurm stands for?
- **S**imple
- **L**ightweight
- **U**niform
- **R**apid
- **M**odular

## What people say
> Funtions. Functions everywhere!

> I'm so higher order!

## Technology
- hyperapp
- tachyons.css
- lisp
- cl-who
- wookie
- parenscript