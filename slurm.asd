;;;; slurm.asd

(asdf:defsystem #:slurm
  :description "Describe slurm here"
  :author "Mathaeus Sander"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (#:wookie #:parenscript #:cl-who #:flexi-streams #:cl-json)
  :components ((:file "slurm")))
